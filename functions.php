<?php
	/*-----------------------------------------------------------------------------------*/
	/* This file will be referenced every time a template/page loads on your Wordpress site
	/* This is the place to define custom fxns and specialty code
	/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Allow SVG through WordPress Media Uploader
/*-----------------------------------------------------------------------------------*/
  function fix_svg_thumb_display() {
    echo 'td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {width: 100% !important;height: auto !important;}';
  }
  add_action('admin_head', 'fix_svg_thumb_display');
/*-----------------------------------------------------------------------------------*/
/* Hide the Admin Bar in WordPress
/*-----------------------------------------------------------------------------------*/
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}
/*-----------------------------------------------------------------------------------*/
/* Favicon
/*-----------------------------------------------------------------------------------*/
function kia_add_favicon(){ ?>
    <!-- Custom Favicons -->
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/dist/images/favicon/favicon.ico"/>
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/favicon/apple-touch-icon.png">
    <?php }
add_action('wp_head','kia_add_favicon');


/** Favicon generator online tool :: http://www.favicon-generator.org/ **/
global $theme_path;
$theme_path = get_stylesheet_directory_uri().'/dist/images/favicon/';
function set_favicons($theme_path)
{
    echo '<link rel="apple-touch-icon" sizes="57x57" href="'.$theme_path.'apple-icon-57x57.png">';
    echo '<link rel="apple-touch-icon" sizes="60x60" href="'.$theme_path.'apple-icon-60x60.png">';
    echo '<link rel="apple-touch-icon" sizes="72x72" href="'.$theme_path.'apple-icon-72x72.png">';
    echo '<link rel="apple-touch-icon" sizes="76x76" href="'.$theme_path.'apple-icon-76x76.png">';
    echo '<link rel="apple-touch-icon" sizes="114x114" href="'.$theme_path.'apple-icon-114x114.png">';
    echo '<link rel="apple-touch-icon" sizes="120x120" href="'.$theme_path.'apple-icon-120x120.png">';
    echo '<link rel="apple-touch-icon" sizes="144x144" href="'.$theme_path.'apple-icon-144x144.png">';
    echo '<link rel="apple-touch-icon" sizes="152x152" href="'.$theme_path.'apple-icon-152x152.png">';
    echo '<link rel="apple-touch-icon" sizes="180x180" href="'.$theme_path.'apple-icon-180x180.png">';
    echo '<link rel="icon" type="image/png" sizes="192x192"  href="'.$theme_path.'android-icon-192x192.png">';
    echo '<link rel="icon" type="image/png" sizes="32x32" href="'.$theme_path.'favicon-32x32.png">';
    echo '<link rel="icon" type="image/png" sizes="96x96" href="'.$theme_path.'favicon-96x96.png">';
    echo '<link rel="icon" type="image/png" sizes="16x16" href="'.$theme_path.'favicon-16x16.png">';
    echo '<link rel="manifest" href="'.$theme_path.'manifest.json">';
    echo '<meta name="msapplication-TileColor" content="#ffffff">';
    echo '<meta name="msapplication-TileImage" content="'.$theme_path.'ms-icon-144x144.png">';
    echo '<meta name="theme-color" content="#ffffff">';
}
/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/
// Google Fonts
function google_fonts() {
  wp_register_style('google_font_lato', 'https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet');
  wp_enqueue_style('google_font_lato');
  wp_register_style('google_font_oswald', 'https://fonts.googleapis.com/css?family=Oswald:300,400,700');
  wp_enqueue_style('google_font_oswald');
}
add_action( 'wp_enqueue_scripts', 'google_fonts');


// styles
function theme_styles() {
  //wp_enqueue_style( 'bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css' );
  //wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/dist/css/vendor/bootstrap.min.css' );
  wp_enqueue_style( 'main_css', get_template_directory_uri() . '/dist/css/main.css' );
  wp_enqueue_style( 'style_css', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_styles');

// scripts
function theme_js() {
	global $wp_scripts;
  //wp_enqueue_script( 'bootstrap_jquery', 'https://code.jquery.com/jquery-3.1.1.slim.min.js', false);
  wp_register_script( 'bootstrap_jquery', get_template_directory_uri() . '/dist/js/vendor/jquery.min.js', __FILE__, '1.1', true);
  wp_enqueue_script('bootstrap_jquery');
  wp_register_script( 'slick_js', get_template_directory_uri() . '/dist/js/vendor/slick.min.js', __FILE__, '1.1', true);
  wp_enqueue_script('slick_js');
  wp_register_script( 'fancybox_js', get_template_directory_uri() . '/dist/js/vendor/fancybox.min.js', __FILE__, '1.1', true);
  wp_enqueue_script('fancybox_js');
	wp_register_script( 'scripts_js', get_template_directory_uri() . '/dist/js/scripts.js', __FILE__, '1.1', true);
  wp_enqueue_script('scripts_js');
}
add_action( 'wp_enqueue_scripts', 'theme_js');
