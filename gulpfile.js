var gulp = require ('gulp');
var uglify = require ('gulp-uglify');
var sass = require('gulp-sass');
const { gulpSassError } = require('gulp-sass-error');
const throwError = true;
var sourcemaps = require('gulp-sourcemaps');
var image = require('gulp-image');
var autoprefixer = require('gulp-autoprefixer');
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};
var cleanDest = require('gulp-clean-dest');
var browserSync = require('browser-sync').create();
var sassOptions = {
  //outputStyle: 'nested'
  //outputStyle: 'compact'
  //outputStyle: 'eÍxpanded'
  outputStyle: 'compressed'
};

// BROWSERSYNC
gulp.task('serve', function() {
    // Static server

    // browserSync.init({
    //     server: {
    //         baseDir: "./"
    //     }
    // });

    // dev server enviroment
    browserSync.init({
        proxy: "wpthemes.dev"
    });
    gulp.watch("source/js/**/*.js").on('change', browserSync.reload);
    gulp.watch("source/scss/**/*.scss").on('change', browserSync.reload);
    gulp.watch("./*.php").on('change', browserSync.reload);
});



// RUNNING SCRIPTS TASKS
gulp.task('compressVendorJS', function () {
  gulp.src('source/js/vendor/*.js')
  .pipe(uglify())
  .pipe(gulp.dest('dist/js/vendor'))
});
gulp.task('compressMainJS', function () {
  gulp.src('source/js/*.js')
  .pipe(cleanDest('dist/js'))
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('dist/js'))
});

gulp.task('scripts', function () {
  gulp.start('compressMainJS');
  gulp.start('compressVendorJS');
});

// RUNNING SASS TASKS
gulp.task('mainSass', function () {
  return gulp.src('source/scss/main.scss')
    .pipe(cleanDest('dist/css'))
    .pipe(sass(sassOptions).on('error', gulpSassError(throwError)))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest('dist/css'));
});
gulp.task('vendorSass', function () {
  return gulp.src('source/scss/vendor/*.scss')
  .pipe(sass(sassOptions).on('error', gulpSassError(throwError)))
  .pipe(sourcemaps.write())
  .pipe(autoprefixer(autoprefixerOptions))
  .pipe(gulp.dest('dist/css/vendor'));
});

gulp.task('minifySass', function () {
  gulp.start('mainSass');
  gulp.start('vendorSass');
});

// RUNNING IMAGES COMPRESSOR
gulp.task('images', function () {
  gulp.src('source/images/*')
    .pipe(cleanDest('dist/images'))
    .pipe(image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      jpegRecompress: false,
      jpegoptim: true,
      mozjpeg: true,
      guetzli: false,
      gifsicle: true,
      svgo: true,
      concurrent: 10
    }))
    .pipe(gulp.dest('dist/images'));
});
// duplicating favicons
gulp.task('favicons', function () {
  gulp.src('source/images/favicon/*.*')
    .pipe(gulp.dest('dist/images/favicon'));
});

gulp.task('runImages', function () {
  gulp.start('images');
  gulp.start('favicons');
});

//WATCHER
gulp.task('watch', function () {
  gulp.watch('source/js/**/*.js', ['scripts']);
  gulp.watch('source/scss/**/*.scss', ['minifySass']);
  gulp.task('browser-sync');
});

// RUN MAIN TASK
gulp.task('default', ['scripts', 'minifySass', 'runImages', 'watch', 'serve'])
