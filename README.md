# GULP PRESS, Starter Wordpress Theme

## Description

GulpPress is a blank WP theme.

## Installation

* Download theme to the themes folder.
* On the terminal place yourself on the GulpPress theme folder ```cd wpProject/wp-content/themes/gulppress```.
* Run ```npm install``` to install dependencies.
* Run ```gulp``` to compile first time your assets.
* Use ```gulp watch``` to update your changes.
* Enjoy
* To minify scss ouptput on dist folder change on gulpfile.js the outputStyle to compressed.
* To add plugins place your scss and js files on source/scss and source/js/vendor folders; after add the paths on the function.php file.
* To stop running ```gulp```, run ```ctrl+c```.
* If your terminal display an error, you will have to run ```gulp``` again.
* Set your assets like scss, js, images files on the ```source``` directory.

## Components

* [Slick slider](http://kenwheeler.github.io/slick/)
* [Fancybox 3](http://fancyapps.com/fancybox/3/)
* [Font Awesome](http://fontawesome.io/)

## Notes

* front-page.php displays sierra.css components
* Observe ```_helpers.scss``` to note some tools like background for banners, text alignments based on device, margins, video wrapper (youtube)
* Typography is based on gridlover.com sizes

## Recommended WP plugins and FrontEnd plugins

* [WP Mobile Detect](https://en-ca.wordpress.org/plugins/wp-mobile-detect/) - Play with this to load images depending of the device type
* [WP Migrate DB](https://en-ca.wordpress.org/plugins/wp-migrate-db/) - Useful to migrate DataBase from local to server or vice versa
* [Slide and Push Menus](https://tympanus.net/codrops/2013/04/17/slide-and-push-menus/) - Fixed menus that will slide out from the sides of the page and in case of the right and left side optionally move the body.
* [Isotope Filter](https://isotope.metafizzy.co/) - Filter & sort magical layouts body.
* [jQuery GRIDDER](https://github.com/oriongunning/gridder 7) - A jQuery plugin that displays a thumbnail grid expanding preview similar to the effect seen on Google Images.
